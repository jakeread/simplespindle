# The ER-Holder Simple Spindle

[log](log/simple-spindle-log.md)

This is a simple spindle for simple milling machines, that we can make with a 3D Printer and some off-the-shelf parts.

It uses a 'collet holder' (which can be found on ebay) with two simple roller bearings on either end to form the main mechanism. See! Simple. Drive that with a BLDC motor, and you're milling. 

![spindle](log/2020-07-08_spindle.png)

## Notes

The belleville washers (disk springs) included in the BOM should be mounted between the collet-holder collet-holding end and the inner race of the bottom bearing. The 40T pulley w/ respective set-screws holds the collet holder into the bearing / body assembly - as you tighten those screws, try to compress those belleville washers. This way the bearings are a little bit preloaded, which helps them run smooth / for a long time / quitely. S/O to [Ilan Moyer](http://web.mit.edu/imoyer/www/index.html) for this tip.

## Runout

I measured a runout at the collet holder of ~ 1 thousandths, and at the tip of a 1/8" tool around ~ 3 thousandths. That's OK for $150 in parts, and a plastic housing.

![spindle test](log/2018-12-07_runout.mp4)

# BOM

This is a short list of parts for the latest design as of **2020 07 10** as implemented on [clank](https://gitlab.cba.mit.edu/jakeread/clank). 

What | Spec | QTY 
--- | --- | --- 
Collet Holder | C8 ER11 100L | 1 
Skate Bearings | 608ZZ or 5972K501 | 2 
Belleville Washers | 96475K331 | 2
Shaft Coupling 5mm Side | 9845T105 | 1 
Shaft Coupling 8mm Side | 9845T108 | 1
Shaft Coupling Guibo | 9845T110 | 1
FHCS | M3x10 | 4 
FHCS | M4x40 | 4 
M4 Locknuts |  |  4
Motor | [NTM 35-36](https://hobbyking.com/en_us/propdrive-v2-3536a-1800kv-brushless-outrunner-motor.html) | 1
