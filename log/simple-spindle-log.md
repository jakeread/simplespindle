## Simple Spindle

## 2020 07 10 

This thing has lived many incarnations, for various machine projects. 

![bldc](2018-10-29_bldc.jpg)
![esc](2018-11-13_esc.png)
![runout](2018-12-07_runout.mp4)
![spindle](2019-12-07_spindle.jpg)
![blue](2019-09-04_blue.jpg)

I've not kept a proper log of them. The basics have been the same: collect holder, two bearings, preload with bellevilles. I've tried various pulley situations, none of which were ever satisfying. Indeed, most were obvious failures once any appreciable loads / speeds were applied. 

SO! I've just done this again for a new machine - Clank - that we are going to try to ship with fab lab home-pandemic kits. I've just given in to the shaft couplings, and a big ol' vertical stack. This is out to order / fab as of today. 

![stack](2020-07-08_spindle.png)
![machine](2020-07-09_clank-lz-render.png)